import { createAccount } from "../../../users/services/user.service"
export default function handler (req, res){
    if (req.method === 'POST'){
        const newUser = createAccount(req.body)
        res.status(200).send(newUser)
    }else {
        res.status(500).send({error: 'Failed'})
    }
}