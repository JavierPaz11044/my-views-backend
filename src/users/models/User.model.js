const bcrypt = require('bcryptjs')
class User {

        constructor(ObjectUserUnformatter){
        this.name = ObjectUserUnformatter.name
        this.last =  ObjectUserUnformatter.last
        this.birtday =  ObjectUserUnformatter.birtday
        this.age =  ObjectUserUnformatter.age
        this.username =  ObjectUserUnformatter.username
        }
}

class Account {
    constructor(ObjectAccountUnformatter){
        this.password = ObjectAccountUnformatter.password
        this.email = ObjectAccountUnformatter.email
    }
}

export class  Profile {
    constructor(ObjectProfileUnformatter){
        this.name = ObjectProfileUnformatter.name
        this.last =  ObjectProfileUnformatter.last
        this.birtday =  ObjectProfileUnformatter.birtday
        this.age =  ObjectProfileUnformatter.age
        this.password = bcrypt.hashSync(ObjectProfileUnformatter.password, 10)
        this.email = ObjectProfileUnformatter.email
        this.username =  (this.name + this.last + this.age)
    }
    getUser(){
        return new User(this)
    }

    getAccount(){
        return new Account(this)
    }

}