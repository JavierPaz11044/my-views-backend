import { PrismaClient } from '@prisma/client'
import { Profile } from '../models/User.model'

const prisma = new PrismaClient()

const createAccount = async (profile) => {
    const newProfile = new Profile(profile)
    console.log(newProfile.getUser())
    const newAccount =  await prisma.account.create({
        data : {
            ...newProfile.getAccount(),
            user : {
                create : newProfile.getUser()
            }
        },
        select : {
            user : true
        }
    })
    return newAccount
}

export {
    createAccount
}